/**
 * サーバー起動及び 必要モジュールをrequire
 *
 * - app/	システム基幹ソース
 * 		- build/	browserifyのbundle先。compress to /public/build/bundle.js
 *    	- sass/		Sassファイル コンパイル先は /public/assets/css/style.css
 *    	- source/	Angular appや Model Controllerなど
 * - public/ パブリックファイル
 * 		- assets/		パブリックファイル
 * 		- index.ejs		メインファイル
 * - node_modules/	Nodeモジュール
 * 
 */
var http = require('http');
var express = require('express');

var app = express();

app.use(express.static('front'));

app.listen(3000);


app.set('views', './public');
app.use(express.static('./public'));


app.get('/', function(req,res){
	 res.render('index.ejs',{
	 	name: 'Mineo Okuda'
	 });
});

