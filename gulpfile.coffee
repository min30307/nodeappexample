babel = require 'gulp-babel';
babelify = require 'babelify';
browserify = require 'browserify';
source = require 'vinyl-source-stream';
buffer = require 'vinyl-buffer';
gulp = require 'gulp';
uglify = require 'gulp-uglify';
insert = require 'gulp-insert';
replace = require 'gulp-replace';
compass = require 'gulp-compass';
plumber = require 'gulp-plumber';
nodemon = require 'gulp-nodemon';
SCSS_FILE = './app/sass/*.scss';

#browserify
gulp.task 'browserify',->
  browserify './app/source/app.js'
    .transform(babelify)
    .bundle()
    .pipe plumber()
    .pipe source('bundle.js')
    .pipe gulp.dest('./app/build/')

# compass
gulp.task 'compass', ->
    gulp.src [SCSS_FILE]
        .pipe plumber()
        .pipe compass 
            config_file: './app/sass/config.rb',
            comments   : false,
            css        : './public/assets/css/',
            sass       : './app/sass/'

#compress
gulp.task 'compress', ->
  gulp.src './app/build/bundle.js'
    .pipe plumber()
    .pipe uglify()
    .pipe gulp.dest('./public/build/')

gulp.task 'core', ->
  gulp.src './app/lib/core.js'
    .pipe plumber()
    .pipe uglify()
    .pipe gulp.dest('./public/assets/js/')


gulp.task 'webserver', ->
  nodemon
    script: 'server.js',
    ext: 'js html',
    env: { 'NODE_ENV': 'development' }

# watch
gulp.task 'watch', ->
    gulp.watch ["./app/source/*.js","./app/source/*/*.js"],['browserify'];
    gulp.watch './app/sass/*.scss', ['compass'];
    gulp.watch ["./app/build/bundle.js"],['compress'];

# default
gulp.task "default",['watch','core','browserify','webserver'], ->  
